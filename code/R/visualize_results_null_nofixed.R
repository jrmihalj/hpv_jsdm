# Authors: Ranjeva et al.
# Description:
## Code for generating figures from the model with no fixed effects. 


################################################
################################################
library(tidyverse)
library(scales)
library(ggsci)
library(rstan)
library(grid)
library(gridExtra)
library(reshape2)

options(dplyr.width = Inf, dplyr.print_max = 1e9)

################################################
################################################



################################################
# Results from NULL NOFIXED postseriors:
################################################


# Import the 3 MCMC chains, stored in SQLite files
setwd("./output/")
filenames <- list.files(pattern = "fit_null_nofixed_chain_")
alphas_db <- data.frame()
Rho_patient_db <- data.frame()
Rho_visit_db <- data.frame()
#betas_phi_db <- data.frame()
#betas_gam_db <- data.frame()
betas_tbv_phi_db <- data.frame()
betas_tbv_gam_db <- data.frame()

# Read in the tables
for( i in c(1:length(filenames))){
  dbResultsFilename <- filenames[i]
  db <- dbConnect(SQLite(), dbResultsFilename)
  alphas_db <- rbind(alphas_db, dbReadTable(db, "alphas"))
  Rho_patient_db <- rbind(Rho_patient_db, dbReadTable(db, "Rho_patient"))
  Rho_visit_db <- rbind(Rho_visit_db, dbReadTable(db, "Rho_visit"))
  #betas_phi_db <- rbind(betas_phi_db, dbReadTable(db, "betas_phi"))
  #betas_gam_db <- rbind(betas_gam_db, dbReadTable(db, "betas_gam"))
  betas_tbv_phi_db <- rbind(betas_tbv_phi_db, dbReadTable(db, "betas_tbv_phi"))
  betas_tbv_gam_db <- rbind(betas_tbv_gam_db, dbReadTable(db, "betas_tbv_gam"))
  dbDisconnect(db)
}

# dimensions (n_strain):
d = 10
n_sample = nrow(alphas_db)

############
############
# Alphas (baseline occurrence probabilities)

alphas <- melt(alphas_db %>% select(-c(chain,sample)))
alphas$strain <- rep(paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), each = n_sample)
alphas <- alphas %>% select(-variable)

plot_alphas =
  ggplot(alphas, aes(x=pnorm(value))) +
  geom_histogram(binwidth=0.0015, linetype = 0, fill="black") + 
  theme_classic() +
  theme(strip.background = element_blank(),
        panel.spacing = unit(1.2, "lines"),
        axis.text = element_text(size = 10, face = "bold", color = "black"),
        axis.title = element_text(size = 10, face = "bold", color = "black"),
        strip.text = element_text(size = 10, face = "bold", color = "black")) + 
  facet_wrap(~strain, nrow=2, scales="free_x") +
  scale_y_continuous(limits=c(0,2400), breaks=c(0,1100,2200)) +
  scale_x_continuous(limits=c(0, 0.1), breaks=seq(0,.08,.04)) +
  labs(x = expression(bold("Baseline occurrence probability, ") ~ bold(alpha)),
       y = "Frequency") 

############
############

# Time between visit effects on persistence and colonization:

## Persistence
betas_tbv_phi <- melt(betas_tbv_phi_db %>% select(-c(chain,sample)))
betas_tbv_phi$strain <- rep(paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), each = n_sample)
betas_tbv_phi$param <- "phi"
betas_tbv_phi <- betas_tbv_phi %>% select(-variable)

## Colonization
betas_tbv_gam <- melt(betas_tbv_gam_db %>% select(-c(chain,sample)))
betas_tbv_gam$strain <- rep(paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), each = n_sample)
betas_tbv_gam$param <- "gamma"
betas_tbv_gam <- betas_tbv_gam %>% select(-variable)

# Combine
tbv <- rbind(betas_tbv_phi, betas_tbv_gam)
tbv <- tbv[!is.na(tbv$strain),]

# which strains have 95% CI that do not overlap zero?
tbv_sigs = tbv %>%
  group_by(strain, param) %>%
  summarize(high = quantile(value, probs=c(0.975)),
            low = quantile(value, probs=c(0.025)),
            median = quantile(value, probs=c(0.5))) %>%
  mutate(sign_high = sign(high),
         sign_low = sign(low)) %>%
  rowwise() %>%
  mutate(sig = ifelse(sign_high*sign_low == 1, 1, 0)) %>%
  mutate(sig_pos = ifelse(sig == 1 & median > 0, 1, 0)) %>%
  filter(sig == 1) %>% 
  select(strain, param, sig_pos)

tbv$sig = 0
for(i in 1:nrow(tbv_sigs)){
  
  val = ifelse(tbv_sigs$sig_pos[i] == 0, 1, 2)
  tbv[tbv$strain == tbv_sigs$strain[i] & tbv$param == tbv_sigs$param[i], 4] = val
  
}

# Plot

plot_tbv =
  ggplot(tbv, aes(x = value, fill=factor(sig))) +
  geom_histogram(binwidth=0.015, linetype=0) + 
  theme_classic() +
  theme(strip.background = element_blank(),
        panel.spacing = unit(1.2, "lines"),
        strip.text.y = element_text(face = "bold", size = 16),
        axis.text = element_text(size = 12, face = "bold", color = "black"),
        axis.title = element_text(size = 14, face = "bold", color = "black"),
        strip.text.x = element_text(size = 12, face = "bold", color = "black")) + 
  guides(fill=F) + 
  facet_grid(param~strain, 
             labeller = labeller(param = label_parsed),
             scales = "free_x") +
  scale_y_continuous(limits=c(0,2000), breaks=c(0,1000,2000)) +
  scale_x_continuous(limits=c(-.6, .3), breaks = c(-.5, 0, 0.3), labels = c("-0.5","0","0.3")) +
  scale_fill_manual(values = c("black", "red", "blue")) +
  labs(x = expression(beta[j]^(TBV)), y = "Frequency") + 
  geom_vline(xintercept = 0, linetype = 3, size = .25)


############
############

# Correlations among patients
Rho_pat <- melt(Rho_patient_db %>% select(-c(chain,sample)))
Rho_pat$strain1 <- rep(rep(paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), each = n_sample), d)
Rho_pat$strain2 <- rep(paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), each = n_sample*d)
corr_pat <- Rho_pat %>% select(-variable)

corr_pat_med = 
  corr_pat %>%
  group_by(strain1, strain2) %>%
  summarize(high = quantile(value, probs=c(0.975)),
            low = quantile(value, probs=c(0.025)),
            median = quantile(value, probs=c(0.5))) %>%
  mutate(sign_high = sign(high),
         sign_low = sign(low)) %>%
  rowwise() %>%
  mutate(sig = ifelse(sign_high*sign_low == 1, 1, 0)) %>%
  mutate(sig_pos = ifelse(sig == 1 & median > 0, 1, 0)) %>%
  select(strain1, strain2, median, sig, sig_pos) %>%
  mutate(med_fixed = ifelse(sig != 1 | median == 1.0, NA, median)) %>%
  mutate(med_fixed = round(med_fixed, 2))

# just upper tri of the matrix
test_mat = matrix(1:100, nrow = 10, ncol = 10)
these = test_mat[lower.tri(test_mat)]

corr_pat_med = corr_pat_med[-these, ]
corr_pat_med$strain1 = as.numeric(as.factor((corr_pat_med$strain1)))
corr_pat_med$strain2 = as.numeric(as.factor((corr_pat_med$strain2)))

plot_corr_pat = 
  ggplot(corr_pat_med, aes(x = strain2, y = strain1, fill = med_fixed)) +
  geom_tile() +
  scale_fill_gradient2("Patient\nCorr", limits = c(-.75,.75), breaks = c(-.75,0,.75),
                       labels = c("-0.75","  0","  0.75"),
                       na.value = "white", low="red", mid = "white", high="blue") +
  theme_classic() +
  scale_y_continuous(breaks = 1:d, 
                     labels = paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), 
                     trans="reverse") +
  scale_x_continuous(breaks = 1:d, 
                     labels = paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)),
                     position = "top") +
  labs(x="", y="") + 
  theme(axis.text.x.top = element_text(angle=30, vjust = 0.5),
        axis.text = element_text(size = 10, face = "bold", color = "black"),
        legend.title = element_text(size = 8, face = "bold"),
        legend.text.align = 1)


############
############

# Correlations among observations, within patients

Rho_vis <- melt(Rho_visit_db %>% select(-c(chain,sample)))
Rho_vis$strain1 <- rep(rep(paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), each = n_sample), d)
Rho_vis$strain2 <- rep(paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), each = n_sample*d)
corr_obs <- Rho_vis %>% select(-variable)

# Summarize (median), and if 95% CI overlaps zero make NA
corr_obs_med = 
  corr_obs %>%
  group_by(strain1, strain2) %>%
  summarize(high = quantile(value, probs=c(0.975)),
            low = quantile(value, probs=c(0.025)),
            median = quantile(value, probs=c(0.5))) %>%
  mutate(sign_high = sign(high),
         sign_low = sign(low)) %>%
  rowwise() %>%
  mutate(sig = ifelse(sign_high*sign_low == 1, 1, 0)) %>%
  mutate(sig_pos = ifelse(sig == 1 & median > 0, 1, 0)) %>%
  select(strain1, strain2, median, sig, sig_pos) %>%
  mutate(med_fixed = ifelse(sig != 1 | median == 1.0, NA, median)) %>%
  mutate(med_fixed = round(med_fixed, 2))

# just upper tri
test_mat = matrix(1:100, nrow = 10, ncol = 10)
these = test_mat[lower.tri(test_mat)]

corr_obs_med = corr_obs_med[-these, ]
corr_obs_med$strain1 = as.numeric(as.factor((corr_obs_med$strain1)))
corr_obs_med$strain2 = as.numeric(as.factor((corr_obs_med$strain2)))

# Plot
plot_corr_obs = 
  ggplot(corr_obs_med, aes(x = strain2, y = strain1, fill = med_fixed)) +
  geom_tile() +
  scale_fill_gradient2("Obs. \nCorr", limits = c(-.75,.75), breaks = c(-.75,0,.75),
                       labels = c("-0.75","  0","  0.75"),
                       na.value = "white", low="red", mid = "white", high="blue") +
  theme_classic() +
  scale_y_continuous(breaks = 1:d, 
                     labels = paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)), 
                     trans="reverse") +
  scale_x_continuous(breaks = 1:d, 
                     labels = paste0("hpv",c(6,11,16,18,31,33,45,52,58,84)),
                     position = "top") +
  labs(x="", y="") + 
  theme(axis.text.x.top = element_text(angle=30, vjust = 0.5),
        axis.text = element_text(size = 10, face = "bold", color = "black"),
        legend.title = element_text(size = 8, face = "bold"),
        legend.text.align = 1)


############
############
# Combine the plots to create Figure 3 

quartz(height = 10, width = 9, dpi = 300)
grid.arrange(plot_alphas, plot_corr_pat, plot_corr_obs, plot_tbv,  
             ncol = 2, nrow = 6,
             layout_matrix = rbind(c(1,1),c(1,1), c(2,3), c(2,3), c(4,4), c(4,4))
)
quartz.save("./figs/combined_null_nofixed_mod.png", type = "png", dpi = 300)

